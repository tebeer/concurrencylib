﻿using Actors;
using System;
using System.Threading.Tasks;

public struct TestMessage
{
    public static TestMessage Create(string name)
    {
        TestMessage msg = new TestMessage();
        msg.Name = name;
        return msg;
    }

    public string Name { get; private set; }
}

public class TestActor : Actor
{
    protected override void Initialize()
    {
        Console.WriteLine("TestActor " + ID);
        //OnMessage<TestMessage>(msg => Console.WriteLine(msg.Name));
        //OnMessage<int>(i => Update());
        //this.EnqueueMessage<int>(0);

        //ActorSystem.GetActorRef<TestActor>(2).EnqueueMessage(TestMessage.Create("Moro2"));

        //sw.Start();

        OnMessage<string>(s =>
            {
                sw.Reset();
                sw.Start();
                count = 0;
            });

        OnMessage<int>(i => 
            {
                //for (int j = 0; j < 10000000; ++j)
                //{ }
                if(i == 1000000-1)
                    Console.WriteLine(ID + ": " + i);
            });
    }

    private async void Update()
    {
        Console.WriteLine("Update " + ID);
        await Task.Delay(1000);
        this.EnqueueMessage<int>(0);
    }

    public int count;
    private System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

}

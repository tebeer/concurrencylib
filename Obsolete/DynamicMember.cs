﻿using System;
using System.Collections.Concurrent;

namespace ConcurrencyLib
{
    internal static class DynamicMember<T> where T : new()
    {
        public static bool TryGetInstanceOf(object owner, out T value)
        {
            Lazy<T> v;
            if (m_instances.TryGetValue(owner, out v))
            {
                value = v.Value;
                return true;
            }
            value = default(T);
            return false;
        }

        public static T InstanceOf(object owner)
        {
            return m_instances.GetOrAdd(owner, o => new Lazy<T>(() => new T())).Value;
        }

        private static ConcurrentDictionary<object, Lazy<T>> m_instances = new ConcurrentDictionary<object, Lazy<T>>();
    }
}
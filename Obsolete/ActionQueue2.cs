﻿
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace ConcurrencyLib
{
    internal class ActionQueue2
    {

        public ActionQueue2(Action init)
        {
            m_first = m_last = new Segment();

            Task.Factory.StartNew(init).ContinueWith(ProcessMessages);
        }

        public bool Add(Action action)
        {
            lock (m_queueLock)
            {
                if (m_last.writeIndex == Segment.Size)
                {
                    Segment e = new Segment();
                    m_last.next = e;
                    m_last = e;
                    //Console.WriteLine("New segment");
                }
                //Console.WriteLine("Write " + m_last.writeIndex);
                m_last.actions[m_last.writeIndex++] = action;
            }

            m_idleToken.Cancel();
            m_idle = false;

            return true;
        }

        private async Task Idle()
        {
            //while (m_queue.Count == 0)
            while (m_idle)
            {
                //Console.WriteLine("Idle");

                var old = Interlocked.Exchange(ref m_idleToken, new CancellationTokenSource());

                try
                {
                    await Task.Delay(-1, m_idleToken.Token).ConfigureAwait(false);
                }
                catch (Exception ex)
                { }

                //if(old != null)
                 //   old.Dispose();
            }
            m_idle = false;
        }

        private async void ProcessMessages(Task task)
        {
            //Console.WriteLine("ProcessMessages");
            sw.Reset();
            sw.Start();

            int processedCount = 0;
            int segmentsCount = 0;
            //Action action;
            //while (m_queue.TryTake(out action))
            //{
            //    action();
            //    processedCount++;
            //}

            //while (!(m_first == m_last && m_first.readIndex == m_first.writeIndex))
            while(true)
            {
                if (m_first.readIndex < m_first.writeIndex)
                {
                    //Console.WriteLine("Read " + m_first.readIndex);
                    m_first.actions[m_first.readIndex++]();
                    processedCount++;
                }
                else
                {
                    if (m_first == m_last)
                        break;

                    //Console.WriteLine("Segment read");
                    segmentsCount++;
                    m_first = m_first.next;
                }
            }

            long elapsed = sw.ElapsedTicks;
            m_totalMessages += processedCount;
            m_totalSegments += segmentsCount;
            m_totalTicks += elapsed;

            if (processedCount > maxCount)
                maxCount = processedCount;

            if((m_totalMessages % 1000000) == 0)
                Console.WriteLine(string.Format("{0} msgs | {1} ticks, tot: {2} | {3} ms max: {4} segments: {5}",
                processedCount, elapsed, m_totalMessages, m_totalTicks * 1000 / Stopwatch.Frequency, maxCount, segmentsCount));

            await Idle().ContinueWith(ProcessMessages);
        }

        private IProducerConsumerCollection<Action> m_queue;

        private Stopwatch sw = new Stopwatch();

        private long maxCount;
        private long m_totalSegments;
        private long m_totalMessages;
        private long m_totalTicks;

        private volatile object m_queueLock = new object();
        private CancellationTokenSource m_idleToken = new CancellationTokenSource();
        private volatile bool m_idle = false;

        private Segment m_first;
        private volatile Segment m_last;

        private class Segment
        {
            public const int Size = 32;
            public Action[] actions = new Action[Size];
            public volatile int readIndex;
            public volatile int writeIndex;
            public Segment next;
        }
    }

}

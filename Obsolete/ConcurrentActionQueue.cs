﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace ConcurrencyLib
{
    public class ConcurrentActionQueue : IActionQueue
    {
        public ConcurrentActionQueue()
        {
            m_queue = new ConcurrentQueue<Action>();
        }

        public void Enqueue(object action)
        {
            m_queue.Enqueue((Action)action);
            m_idle.Cancel();
        }

        public void Start()
        {
            Task.Factory.StartNew(() => {}).ContinueWith(ProcessMessages);
        }

        public void Stop()
        {
        }

        private object m_idleLock = new object();

        private async Task Idle()
        {
            try
            {
                await Task.Delay(-1, m_idle.Token).ConfigureAwait(false);
            }
            finally
            {
                var old = m_idle;
                m_idle = new CancellationTokenSource();

                if (old != null)
                    old.Dispose();
            }
        }

        private async void ProcessMessages(Task task)
        {
            //Console.WriteLine("ProcessMessages");
            sw.Reset();
            sw.Start();

            int processedCount = 0;

            Action action;
            while (m_queue.TryDequeue(out action))
            {
                action();
                processedCount++;
            }

            long elapsed = sw.ElapsedTicks;
            m_totalMessages += processedCount;
            m_totalTicks += elapsed;

            m_processTimes++;

            if((m_totalMessages % 1000000) == 0)
            Console.WriteLine(string.Format("{0} msgs | {1} ticks, tot: {2} | {3} ms times: {4}",
                processedCount, elapsed, m_totalMessages, m_totalTicks * 1000 / Stopwatch.Frequency, m_processTimes));

            await Idle().ContinueWith(ProcessMessages).ConfigureAwait(false);
        }

        private volatile ConcurrentQueue<Action> m_queue;

        private Stopwatch sw = new Stopwatch();

        private long m_processTimes;
        private long m_totalMessages;
        private long m_totalTicks;

        private volatile CancellationTokenSource m_idle = new CancellationTokenSource();

    }

}

﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace ConcurrencyLib
{
    public class ActionQueue : IActionQueue
    {
        public void Start()
        {
            m_pendingList = new List<Action>(1024);
            m_processingList = new List<Action>(1024);
        }

        public void Stop()
        {
        }

        public void Enqueue(object action)
        {
            bool process = false;
            lock (m_queueLock)
            {
                m_pendingList.Add((Action)action);
                if(!waitingToProcess)
                {
                    waitingToProcess = true;
                    process = true;
                }
            }
            if(process)
                ThreadPool.QueueUserWorkItem(new WaitCallback(ProcessMessages));
        }

        private void ProcessMessages(object state)
        {
            sw.Reset();
            sw.Start();

            lock(m_queueLock)
            {
                if (m_pendingList.Count == 0)
                {
                    waitingToProcess = false;
                    return;
                }

                // Swap the lists
                // ProcessingList is empty
                var temp = m_processingList;
                m_processingList = m_pendingList;
                m_pendingList = temp;
            }

            int processedCount = m_processingList.Count;

            for (int i = 0; i < processedCount; ++i)
                m_processingList[i]();

            m_processingList.Clear();

            long elapsed = sw.ElapsedTicks;
            m_totalMessages += processedCount;
            m_totalTicks += elapsed;

            m_processTimes++;

            if((m_totalMessages % 1000000) == 0)
            Console.WriteLine(string.Format("{0} msgs | {1} ticks, tot: {2} | {3} ms times: {4}",
                processedCount, elapsed, m_totalMessages, m_totalTicks * 1000 / Stopwatch.Frequency, m_processTimes));

            lock (m_queueLock)
            {
                if (m_pendingList.Count == 0)
                    waitingToProcess = false;
                else
                    ThreadPool.QueueUserWorkItem(new WaitCallback(ProcessMessages));
            }
        }

        private Stopwatch sw = new Stopwatch();

        private long m_processTimes;
        private long m_totalMessages;
        private long m_totalTicks;

        private readonly object m_queueLock = new object();
        private List<Action> m_pendingList;
        private List<Action> m_processingList;
        private bool waitingToProcess;
    }

}

﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace ConcurrencyLib
{
    internal class ConcQueue<T>
    {
        public ConcQueue()
        {
            m_first = m_last = new Element();
        }

        private object queueLock = new object();

        public bool TryAdd(T item)
        {
            Element e = new Element();
            e.item = item;

            lock (queueLock)
            {
                m_last.next = e;
                m_last = e;
            }

            return true;
        }

        public bool TryTake(out T item)
        {
            // No need for locks since this is synchronized
            if (m_first.next == null)
            {
                item = default(T);
                return false;
            }
            else
            {
                item = m_first.next.item;
                m_first = m_first.next;
            
                return true;
            }
        }

        public IEnumerable<T> DequeueAll()
        {
            while(m_first != m_last)
            {
                m_first = m_first.next;
                yield return m_first.item;
            }
        }

        private Element m_first;
        private Element m_last;

        private class Element
        {
            public T item;
            public Element next;
        }
    }

}

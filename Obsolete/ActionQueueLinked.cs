﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace ConcurrencyLib
{
    public class ActionQueueLinked : IActionQueue
    {
        public void Start()
        {
            m_pendingList = new LinkedArray<Action>();
            m_processingList = new LinkedArray<Action>();
        }

        public void Stop()
        {
        }

        public void Enqueue(object action)
        {
            bool process = false;
            lock (m_queueLock)
            {
                m_pendingList.Add((Action)action);
                if(!waitingToProcess)
                {
                    waitingToProcess = true;
                    process = true;
                }
            }
            if(process)
                ThreadPool.QueueUserWorkItem(new WaitCallback(ProcessMessages));
        }

        private void ProcessMessages(object state)
        {
            sw.Reset();
            sw.Start();

            lock(m_queueLock)
            {
                if (m_pendingList.Count == 0)
                {
                    waitingToProcess = false;
                    return;
                }

                // Swap the lists
                // ProcessingList is empty
                var temp = m_processingList;
                m_processingList = m_pendingList;
                m_pendingList = temp;
            }

            int processedCount = m_processingList.Count;

            for (var i = m_processingList.GetIterator(); i.Next();)
            {
                i.Current()();
            }
            //foreach (var a in m_processingList)
            //    a();
            //for (int i = 0; i < processedCount; ++i)
            //    m_processingList[i]();

            m_processingList.Reset();

            long elapsed = sw.ElapsedTicks;
            m_totalMessages += processedCount;
            m_totalTicks += elapsed;

            m_processTimes++;

            if((m_totalMessages % 1000000) == 0)
            Console.WriteLine(string.Format("{0} msgs | {1} ticks, tot: {2} | {3} ms times: {4} node count: {5}",
                processedCount, elapsed, m_totalMessages, m_totalTicks * 1000 / Stopwatch.Frequency, m_processTimes, m_processingList.nodeCount));

            lock (m_queueLock)
            {
                if (m_pendingList.Count == 0)
                    waitingToProcess = false;
                else
                    ThreadPool.QueueUserWorkItem(new WaitCallback(ProcessMessages));
            }
        }

        private Stopwatch sw = new Stopwatch();

        private long m_processTimes;
        private long m_totalMessages;
        private long m_totalTicks;

        private readonly object m_queueLock = new object();
        private LinkedArray<Action> m_pendingList;
        private LinkedArray<Action> m_processingList;
        private bool waitingToProcess;
    }

}

﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace ConcurrencyLib
{
    internal class MessageQueue //where T : IProducerConsumerCollection<Action>, new()
    {
        public MessageQueue(Action init)
        {
            Task.Factory.StartNew(init).ContinueWith(ProcessMessages);

        }

        public void Enqueue(Action msg)
        {
            m_collection.TryAdd(msg);
            m_idle.Cancel();
        }

        private Stopwatch sw = new Stopwatch();

        private async void ProcessMessages(Task task)
        {
            //Console.WriteLine("ProcessMessages");
            sw.Reset();
            sw.Start();

            int processedCount = 0;

            Action msg;
            while (m_collection.TryTake(out msg))
            {
                msg();
                processedCount++;
            }

            long elapsed = sw.ElapsedTicks;
            m_totalMessages += processedCount;
            m_totalTicks += elapsed;
            if(m_totalMessages == 4000000)
            Console.WriteLine(string.Format("Processed {0} messages in {1} ticks, Total: {2} messages in {3} ms",
                processedCount, elapsed, m_totalMessages, m_totalTicks * 1000 / Stopwatch.Frequency));

            try
            {
                sw.Reset();
                sw.Start();
                m_idle = new CancellationTokenSource();
                var idleTask = Task.Delay(-1, m_idle.Token).ContinueWith(ProcessMessages);
                await idleTask;
                //Console.WriteLine("Idled for " + sw.ElapsedMilliseconds + " ms");
            }
            catch(Exception ex)
            {
                Console.Write("Cancel");
                //idle.Dispose();
            }
        }

        private long m_totalMessages;
        private long m_totalTicks;

        private CancellationTokenSource m_idle = new CancellationTokenSource();
        //private IProducerConsumerCollection<Action> m_collection = new ConcurrentQueue<Action>();
        private ConcQueue<Action> m_collection = new ConcQueue<Action>();
        //private ArrayQueue<Action> m_collection = new ArrayQueue<Action>();
    }

    //public class MessageQueue : MessageQueueBase
    //{
    //    public MessageQueue(Action init) : base(init)
    //    {
    //    }
    //
    //    protected override void EnqueueMessage(Action msg)
    //    {
    //        m_queue.Enqueue(msg);
    //        //m_bag.Add(() => HandlerOf<T>.TryHandle(this, message));
    //    }
    //
    //    protected override Action DequeueMessage()
    //    {
    //        Action a = null;
    //        m_queue.TryDequeue(out a);
    //        return a;
    //    }
    //
    //    private ConcurrentQueue<Action> m_queue = new ConcurrentQueue<Action>();
    //    private ConcurrentBag<Action> m_bag = new ConcurrentBag<Action>();
    //}
}

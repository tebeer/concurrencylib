﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace ConcurrencyLib
{
    public class ActionQueueOld : IActionQueue
    {
        public void Start()
        {
            m_first = m_last = new Element();
            Task.Factory.StartNew(() => { }).ContinueWith(ProcessMessages);
        }

        public void Stop()
        {
        }

        public void Enqueue(object action)
        {
            Element e = new Element();
            e.action = (Action)action;

            var spin = new SpinWait();
            while (true)
            {
                Element last = m_last;
                if (Interlocked.CompareExchange(ref m_last, e, m_last) == last)
                {
                    last.next = e;
                    break;
                }
                spin.SpinOnce();
            }

            //lock (m_queueLock)
            //{
            //    m_last.next = e;
            //    m_last = e;
            //}

            m_idle.Cancel();
        }

        private object m_idleLock = new object();

        private async Task Idle()
        {
            try
            {
                await Task.Delay(-1, m_idle.Token).ConfigureAwait(false);
            }
            finally
            {
                var old = m_idle;
                m_idle = new CancellationTokenSource();

                if (old != null)
                    old.Dispose();
            }
        }

        private async void ProcessMessages(Task task)
        {
            //Console.WriteLine("ProcessMessages");
            sw.Reset();
            sw.Start();

            int processedCount = 0;

            var spin = new SpinWait();
            while (m_first != m_last)
            {
                if (m_first.next == null)
                {
                    // next == null, this means a new element is coming
                    spin.SpinOnce();
                    continue;
                }

                m_first = m_first.next;
                m_first.action();
                processedCount++;
            }

            long elapsed = sw.ElapsedTicks;
            m_totalMessages += processedCount;
            m_totalTicks += elapsed;

            m_processTimes++;

            if((m_totalMessages % 1000000) == 0)
            Console.WriteLine(string.Format("{0} msgs | {1} ticks, tot: {2} | {3} ms times: {4}",
                processedCount, elapsed, m_totalMessages, m_totalTicks * 1000 / Stopwatch.Frequency, m_processTimes));

            await Idle().ContinueWith(ProcessMessages);
        }

        private Stopwatch sw = new Stopwatch();

        private long m_processTimes;
        private long m_totalMessages;
        private long m_totalTicks;

        private CancellationTokenSource m_idle = new CancellationTokenSource();

        private Element m_first;
        private Element m_last;

        private class Element
        {
            public volatile Action action;
            public volatile Element next;
        }
    }

}

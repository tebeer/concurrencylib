﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcurrencyLib
{
    public class LinkedArray<T> : IEnumerable<T>
    {

        public LinkedArray()
        {
            m_head = m_tail = new Node();
            for (int i = 0; i < 10 * NODE_SIZE; ++i)
                Add(default(T));
            Reset();
        }

        public void Add(T element)
        {
            m_tail.array[m_index++] = element;
            m_count++;
            if (m_index == NODE_SIZE)
            {
                m_index = 0;
                if (m_tail.next == null)
                {
                    m_tail.next = new Node();
                    nodeCount++;
                }

                m_tail = m_tail.next;
            }
        }

        public void Reset()
        {
            m_tail = m_head;
            m_count = 0;
            m_index = 0;
        }

        public int Count { get { return m_count; } }

        public Iterator GetIterator()
        {
            return new Iterator(this);
        }

        public struct Iterator
        {
            private int index;
            private int totalIndex;
            private Node current;

            private LinkedArray<T> m_list;

            public Iterator(LinkedArray<T> list)
            {
                m_list = list;
                index = 0;
                totalIndex = 0;
                current = list.m_head;
            }

            public T Current()
            {
                return current.array[index];
            }

            public bool Next()
            {
                if (++totalIndex == m_list.m_count)
                    return false;

                if (++index == NODE_SIZE)
                {
                    current = current.next;
                    index = 0;
                }
                return true;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            Node current = m_head;
            int c = 0;
            for (int i = 0; i < m_count; ++i)
            {
                yield return current.array[c];

                if (++c == NODE_SIZE)
                {
                    current = current.next;
                    c = 0;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private Node m_head;
        private Node m_tail;

        private int m_count;
        private int m_index;

        public int nodeCount;

        const int NODE_SIZE = 1024;
        private class Node
        {
            public Node next;
            public T[] array = new T[NODE_SIZE];
        }
    }
}

﻿using System.Threading.Tasks;

namespace ConcurrencyLib
{
    public struct ActorRef
    {
        internal ActorRef(Actor actor)
        {
            m_actor = actor;
        }

        public int ID { get { return m_actor == null ? 0 : m_actor.ID; } }
        public bool IsValid
        {
            get
            {
                if (m_actor == null)
                    return false;
                if (m_actor.Disposed)
                {
                    m_actor = null;
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Send a message to actor
        /// </summary>
        /// <typeparam name="T">Message type</typeparam>
        /// <param name="message">The message to be sent</param>
        /// <returns>True if the message was enqueued, false otherwise.</returns>
        public bool EnqueueMessage<T>(T message)
        {
            if (m_actor == null)
                return false;

            if (m_actor.Disposed)
            {
                m_actor = null;
                return false;
            }

            return m_actor.EnqueueMessage(message);
        }

        /// <summary>
        /// Send a message to actor
        /// </summary>
        /// <typeparam name="T">Message type</typeparam>
        /// <param name="message">The message to be sent</param>
        /// <returns>Returns a task which completes with true if the message is handled by target actor, 
        /// or false immediately if the message was not enqueued.</returns>
        public async Task<bool> EnqueueMessageAsync<T>(T message)
        {
            if (m_actor == null)
                return false;

            if (m_actor.Disposed)
            {
                m_actor = null;
                return false;
            }

            TaskCompletionSource<bool> source = new TaskCompletionSource<bool>();

            if (!m_actor.EnqueueMessageAsync(message, source))
                return false;

            await source.Task;

            return true;
        }

        private Actor m_actor;
    }
}

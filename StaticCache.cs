﻿namespace ConcurrencyLib
{
    public static class StaticCache<T> where T : Actor, new()
    {
        public static int Count()
        {
            return cache.Count();
        }

        public static ActorRef GetOrCreateActor(int id)
        {
            return cache.GetOrCreateActor(id);
        }

        public static bool TryGetActor(int id, out ActorRef actorRef)
        {
            return cache.TryGetActor(id, out actorRef);
        }

        public static ActorRef CreateActor()
        {
            return cache.CreateActor();
        }
        
        public static bool ActorExists(int id)
        {
            return cache.ActorExists(id);
        }
        
        // Static caches are created on application start
        private static ActorCache<T> cache = new ActorCache<T>(null);
    }
}

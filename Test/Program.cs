﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConcurrencyLib;
using System.Threading;
using System.Threading.Tasks;
using ExitGames.Concurrency.Fibers;

class Program
{

    private static void TestConcurrent(ActorSystem system, int id)
    {
        Console.WriteLine("TestConcurrent " + id);
        var sw = new System.Diagnostics.Stopwatch();
        var a1 = system.GetOrCreateActor<TestActor>(id);
        sw.Start();
        for (int i = 0; i < 1000000; ++i)
        {
            if (i == 500000)
            {
                a1.EnqueueMessage("pause");
            }
            a1.EnqueueMessage(i);
        }
        Console.WriteLine("Queued " + id + " in " + sw.ElapsedMilliseconds);
    }

    private static void TestSinglethread(ActorSystem system, int id) 
    {
        Console.WriteLine("TestSinglethread " + id);
        var sw = new System.Diagnostics.Stopwatch();
        var a1 = system.GetOrCreateActor<TestActor>(id);

        long totalTicks = 0;
        int count = 10;

        for (int i = 0; i < count; ++i)
        {
            sw.Reset();
            sw.Start();
            for (int j = 0; j < 1000000; ++j)
                a1.EnqueueMessage(i);
            totalTicks += sw.ElapsedTicks;
        }
        totalTicks = 1000 * totalTicks / System.Diagnostics.Stopwatch.Frequency;
        long avgMsec = totalTicks / count;
        Console.WriteLine("Queued " + id + " in " + totalTicks + " avg: " + avgMsec);
    }

    static void Main(string[] args)
    {
        Console.WriteLine("MOro");

        //ActorSystem system = new ActorSystem();

        //Console.WriteLine(ThreadPool.SetMinThreads(4, 4));
        //Console.WriteLine(ThreadPool.SetMaxThreads(4, 4));

        var a1 = StaticCache<TestActor>.GetOrCreateActor(1);
        a1.EnqueueMessage(1);
        //var a2 = system.GetActorRef<TestActor<FiberQueue>>(2);
        //
        //for (int i = 0; i < 100; ++i)
        //{
        //    if (i == 50)
        //    {
        //        a1.EnqueueMessage("async");
        //        a2.EnqueueMessage("async");
        //    }
        //    a1.EnqueueMessage(i);
        //    a2.EnqueueMessage(i);
        //}
        //
        //Console.WriteLine("Done");

        //Parallel.Invoke(
        //    () => TestConcurrent(system, 1),
        //    () => TestConcurrent(system, 2),
        //    () => TestConcurrent(system, 3),
        //    () => TestConcurrent(system, 4),
        //    () => TestConcurrent(system, 5),
        //    () => TestConcurrent(system, 6)
        //    );
        //Thread.Sleep(2000);
        //TestAsync(system);

        Console.WriteLine();
        //TestSinglethread<FiberQueue>(system, 2);
        //Thread.Sleep(2000);
        //
        //Console.WriteLine();
        //Parallel.Invoke(
        //    () => TestConcurrent<ActionQueue>(system, 1),
        //    () => TestConcurrent<ActionQueue>(system, 1),
        //    () => TestConcurrent<ActionQueue>(system, 1),
        //    () => TestConcurrent<ActionQueue>(system, 1),
        //    () => TestConcurrent<ActionQueue>(system, 1),
        //    () => TestConcurrent<ActionQueue>(system, 1)
        //    );
        //Thread.Sleep(2000);
        //
        //Console.WriteLine();
        //TestSinglethread<ActionQueue>(system, 2);

        //a.EnqueueMessage(TestMessage.Create("moro"));
        //a.EnqueueMessage(TestMessage.Create("mo4ro"));
        Console.ReadLine();
    }

    private static async Task TestAsync(ActorSystem system)
    {
        Console.WriteLine("TestAsync");
        var a1 = system.GetOrCreateActor<TestActor>(1);
        Console.WriteLine("Send");
        //await a1.EnqueueMessageAsync("moro");
        Console.WriteLine("Done");

    }
}

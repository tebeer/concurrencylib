﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ConcurrencyLib;

namespace Test
{
    public class GamePeer
    {
        static int userID;

        public GamePeer()
        {

            userID++;

            m_actor = GameServer.ActorSystem.GetOrCreateActor<GameActor>(userID);

            var room = GameServer.ActorSystem.CreateActor<GameRoom>();
            Console.WriteLine("Room created:" + room.ID);

            m_actor.EnqueueMessage(new JoinRoom(room.ID));

            m_rand = new Random();
            Task.Factory.StartNew(Receive);
        }

        private async void Receive()
        {
            while (true)
            {
                await Task.Delay(m_rand.Next(50, 80));
                Console.WriteLine("Send");

                m_actor.EnqueueMessage(new RoomMessage());
            }
        }

        private Random m_rand;
        private ActorRef m_actor;
    }
}

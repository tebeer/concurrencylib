﻿using ConcurrencyLib;
using System;

namespace Test
{

    public class GameActor : Actor
    {
        protected override void OnInitialize()
        {
            Console.WriteLine("GameActor.Initialize");

            Handle<JoinRoom>(OnJoinRoom);

            Handle<RoomMessage>(OnRoomMessage);
            Handle<int>(OnInt);
        }

        protected override void OnDestroy()
        {
        }

        protected override void OnException(Exception ex)
        {
        }

        private void OnJoinRoom(JoinRoom join)
        {
            if (!GameServer.ActorSystem.ActorExists<GameRoom>(join.roomID))
            {
                Console.WriteLine("Unknown room: " + join.roomID);
                return;
            }
            m_room = GameServer.ActorSystem.GetOrCreateActor<GameRoom>(join.roomID);
            Console.WriteLine(ID + " joined room " + join.roomID);
        }
        
        private void OnRoomMessage(RoomMessage msg)
        {
            m_room.EnqueueMessage<RoomMessage>(msg);
        }

        private void OnInt(int i)
        {
            if (i == 1)
            {
//                GameServer.ActorSystem.GetActorRef<GameRoom>();
            }
            Console.WriteLine("OnInt " + i);
        }

        private ActorRef m_room;
    }
}

﻿using ConcurrencyLib;

namespace Test
{
    public struct CreateRoom
    {

    }

    public struct JoinRoom
    {
        public readonly int roomID;
        public JoinRoom(int id)
        {
            roomID = id;
        }
    }

    public struct RoomMessage
    {
        public readonly ActorRef sender;

        public RoomMessage(ActorRef sender)
        {
            this.sender = sender;
        }
    }

}

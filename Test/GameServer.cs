﻿using System;
using System.Collections.Generic;
using ConcurrencyLib;

namespace Test
{
    public class GameServer
    {
        public static readonly ActorSystem ActorSystem = new ActorSystem();
        
        static void Main2(string[] args)
        {
            Console.WriteLine("Main");


            ActorSystem.CreateActor<GameRoom>();

            CreatePeer();

            Console.ReadKey();
        }
        
        private static GamePeer CreatePeer()
        {
            return new GamePeer();
        }

    }
}

﻿using ConcurrencyLib;
using System;
using System.Threading.Tasks;

public struct TestMessage
{
    public static TestMessage Create(string name)
    {
        TestMessage msg = new TestMessage();
        msg.Name = name;
        return msg;
    }

    public string Name { get; private set; }
}

public class CustomException : Exception
{
    public CustomException(string msg) : base(msg)
    {
    }
}

public class TestActor : Actor
{

    protected override void InitMessageQueue()
    {
        SetFixedUpdate(30, Update);
    }

    protected override void OnInitialize()
    {
        //Console.WriteLine("TestActor " + ID + " " + typeof(T).ToString());
        //OnMessage<TestMessage>(msg => Console.WriteLine(msg.Name));
        //OnMessage<int>(i => Update());
        //this.EnqueueMessage<int>(0);

        //ActorSystem.GetActorRef<TestActor>(2).EnqueueMessage(TestMessage.Create("Moro2"));

        //sw.Start();

        //if (ID == 3)
        //{
        //    var r = ActorSystem.GetActorRef<TestActor>(2);
        //    for (int i = 0; i < 1000000; ++i)
        //        r.EnqueueMessage(i);
        //}

        //Schedule(() => Console.WriteLine("After 1000"), 1000);
        //update = ScheduleOnInterval(Update, 30);
        startTime = Environment.TickCount;
        Console.WriteLine("StarTime: " + startTime);
        Handle<string>(OnString);
        //HandleAsync<int>(OnInt);

        //System.Threading.Thread.Sleep(1000);
            //update.Dispose();
    }
    protected override void OnDestroy()
    {
    }
    protected override void OnException(Exception ex)
    {

        //Console.WriteLine(GetType().ToString() + " " + ID + " exception: " + ex.Message);
    }


    private IDisposable update;

    private void Update(int ms)
    {
        int time = Environment.TickCount;
        tick++;
        if(tick == 4)
        {
            int dTime = time - startTime;
            startTime = time;
            Console.WriteLine(tick + " Ticks / sec " + (1000.0f * tick / dTime).ToString("0.00") + " " + time);
            tick = 0;
        }
    }
    private int tick;
    private int startTime;

    private void OnString(string s)
    {
        Console.WriteLine(ID + ": " + s);
        if (s == "destroy")
        {
            Destroy();
            return;
        }

        //sw.Reset();
        //sw.Start();
        //throw new CustomException(s);
        //Console.WriteLine(ID + ": " + s + " Finish " + count++);
    }

    private async Task OnInt(int i)
    {
        //if (i == 1)
        //{
        //    var a2 = StaticCache<TestActor>.GetOrCreateActor(2);
        //    try
        //    {
        //        Console.WriteLine("Send destroy");
        //        for (int j = 0; j < 1000000; ++j)
        //        {
        //            //Console.WriteLine("Send " + j);
        //            if (j == 5)
        //            {
        //                await a2.EnqueueMessageAsync("destroy");
        //            }
        //            else if (!await a2.EnqueueMessageAsync(j.ToString()))
        //            {
        //                throw new Exception("Enqueue failed");
        //            }
        //
        //            //Task t;
        //            //if (j == 5)
        //            //{
        //            //    bool send = a2.EnqueueMessageAsync("destroy", out t);
        //            //    Console.WriteLine("destroy " + send);
        //            //}
        //            //else
        //            //{
        //            //    bool send = a2.EnqueueMessageAsync(j.ToString(), out t);
        //            //    Console.WriteLine(j.ToString() + " " + send);
        //            //    if(!send)
        //            //        throw new Exception("Enqueue failed");
        //            //}
        //        }
        //        //await Task.Delay(100);
        //        //Console.WriteLine("Send Moro!");
        //        //bool success = await a2.EnqueueMessageAsync("Moro!");
        //        //Console.WriteLine(success);
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("ex: " + ex.ToString());
        //    }
        //}

        //if(i % 100000 == 0)
            Console.WriteLine(ID + ": " + i);
        //for (int j = 0; j < 10000000; ++j)
        //{ }
        //if(i == 1000000-1)
        //    Console.WriteLine(ID + " " + typeof(T).ToString() + ": " + i);

        //if (i != lastMsg + 1)
        //{
        //    Console.WriteLine("Error: " + lastMsg + " => " + i); 
        //}
        //lastMsg = i;
    }

    public int lastMsg = -1;
    private int count = 0;
    private System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

}

﻿using ConcurrencyLib;
using System;

namespace Test
{
    public class GameRoom : Actor
    {
        protected override void OnInitialize()
        {
            Handle<RoomMessage>(OnRoomMessage);
        }
        protected override void OnDestroy()
        {
        }
        protected override void OnException(Exception ex)
        {
        }


        private void OnRoomMessage(RoomMessage msg)
        {
            Console.WriteLine("OnRoomMessage");
        }

    }
}

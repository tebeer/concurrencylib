﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ConcurrencyLib
{
    public abstract class Actor
    {
        public int ID { get { return m_id; } }

        internal bool Disposed { get { return m_disposed; } }

        internal static T CreateActor<T>(ActorCache<T> cache, int id) where T : Actor, new()
        {
            T t = new T();
            t.m_id = id;
            t.m_cache = cache;

            t.InitMessageQueue();
            t.Enqueue(t.OnInitialize);
            return t;
        }

        /// <summary>
        /// Starts the destroy process.
        /// Prevents further messages to be enqueued and adds OnDestroy as the last message to be processed.
        /// All messages currently in queue will still be processed.
        /// </summary>
        protected void Destroy()
        {
            if (m_disposed)
                return;

            m_disposed = true;

            // Remove from cache
            m_cache.Remove(ID);

            // Handle destroy as the last action and stop messagequeue
            m_messageQueue.Enqueue(HandleDestroy);
            m_messageQueue.Stop();
        }

        private Task HandleDestroy()
        {
            OnDestroy();

            // Dispose timers
            for (int i = 0; i < m_timers.Count; ++i)
            {
                var handle = m_timers[i];
                handle.registeredWaitHandle.Unregister(handle.waitHandle);
            }
            m_timers.Clear();

            m_messageQueue.Dispose();

            return null;
        }

        internal bool EnqueueMessage<T>(T message)
        {
            return m_messageQueue.Enqueue(() => 
            {
                try
                {
                    object o;
                    if (m_handlers.TryGetValue(typeof(T), out o))
                    {
                        // Check if it's sync handler
                        var action = o as Action<T>;
                        if (action != null)
                        {
                            action(message);
                            return null;
                        }

                        // Check if it's async handler
                        var func = o as Func<T, Task>;
                        if (func != null)
                        {
                            return func(message).ContinueWith((t) =>
                            {
                                // Check if async handler threw exception
                                if (t.IsFaulted)
                                    OnException(t.Exception.InnerException);
                            });
                        }
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    // Exception caught from sync message handler
                    OnException(ex);
                    return null;
                }
            });
        }

        internal bool EnqueueMessageAsync<T>(T message, TaskCompletionSource<bool> source)
        {
            return m_messageQueue.Enqueue(() =>
            {
                try
                {
                    object o;
                    if (m_handlers.TryGetValue(typeof(T), out o))
                    {
                        // Check if it's sync handler
                        var action = o as Action<T>;
                        if (action != null)
                        {
                            action(message);
                            source.TrySetResult(true);
                            return null;
                        }

                        // Check if it's async handler
                        var func = o as Func<T, Task>;
                        if (func != null)
                        {
                            return func(message).ContinueWith((t) =>
                            {
                                // Check if async handler threw exception and send it back to source
                                if (t.IsFaulted)
                                {
                                    var e = t.Exception.InnerException;
                                    source.SetException(e);
                                    OnException(e);
                                }
                                else
                                    source.TrySetResult(true);
                            });
                        }
                    }

                    // Message was not handled
                    source.SetResult(false);
                    return null;
                }
                catch (Exception ex)
                {
                    // Exception caught from SYNC message handler
                    // Send it back to source
                    source.SetException(ex);
                    OnException(ex);
                    return null;
                }
            });
        }

        protected abstract void OnInitialize();
        protected abstract void OnDestroy();
        protected abstract void OnException(Exception ex);
        protected virtual void InitMessageQueue()
        {
            if (m_messageQueue == null)
                m_messageQueue = new MessageQueue();
        }

        protected void SetFixedUpdate(int intervalMS, Action<int> updateFunc)
        {
            if (m_messageQueue != null)
                throw new Exception("MessageQueue already created");

            m_messageQueue = new MessageQueueFixed(intervalMS, updateFunc);
        }

        protected void Handle<T>(Action<T> handlerFunc)
        {
            m_handlers[typeof(T)] = handlerFunc;
        }

        protected void HandleAsync<T>(Func<T, Task> asyncFunc)
        {
            m_handlers[typeof(T)] = asyncFunc;
        }



        protected void Enqueue(Action action)
        {
            m_messageQueue.Enqueue(() => { action(); return null; });
        }

        protected void EnqueueAsync(MessageHandler func)
        {
            m_messageQueue.Enqueue(func);
        }

        protected IDisposable Schedule(Action action, int firstInMs)
        {
            //TimerHandle handle = new TimerHandle();
            //handle.DisposeTimer = DisposeTimer;
            //handle.timer = new Timer(t =>
            //{
            //    m_messageQueue.Enqueue(() =>
            //    {
            //        action();
            //        DisposeTimer(handle);
            //        return null;
            //    });
            //}, null, firstInMs, Timeout.Infinite);
            //
            //m_timers.Add(handle);

            TimerHandle handle = new TimerHandle();
            handle.DisposeTimer = DisposeTimer;
            m_timers.Add(handle);

            handle.waitHandle = new AutoResetEvent(false);
            handle.registeredWaitHandle = ThreadPool.RegisterWaitForSingleObject(handle.waitHandle, (state, timeout) => 
            {
                m_messageQueue.Enqueue(() =>
                {
                    action();
                    DisposeTimer(handle);
                    return null;
                });
            },  null, firstInMs, true);

            return handle;
        }

        protected IDisposable ScheduleOnInterval(Action action, int interval)
        {
            //TimerHandle handle = new TimerHandle();
            //handle.DisposeTimer = DisposeTimer;
            //handle.timer = new Timer(t =>
            //{
            //    m_messageQueue.Enqueue(() =>
            //    {
            //        action();
            //        return null;
            //    });
            //}, null, firstInMs, interval);

            TimerHandle handle = new TimerHandle();
            handle.DisposeTimer = DisposeTimer;
            m_timers.Add(handle);

            handle.waitHandle = new AutoResetEvent(false);
            handle.registeredWaitHandle = ThreadPool.RegisterWaitForSingleObject(handle.waitHandle, (state, timeout) => 
            {
                m_messageQueue.Enqueue(() =>
                {
                    action();
                    return null;
                });
            },  null, interval, false);

            return handle;
        }

        private void DisposeTimer(TimerHandle handle)
        {
            handle.registeredWaitHandle.Unregister(handle.waitHandle);
            m_timers.Remove(handle);
        }

        private Dictionary<Type, object> m_handlers = new Dictionary<Type, object>();

        private int m_id;
        private ActorCacheBase m_cache;
        private IMessageQueue m_messageQueue;
        private bool m_disposed;

        private class TimerHandle : IDisposable
        {
            //public IDisposable timer;
            public WaitHandle waitHandle;
            public RegisteredWaitHandle registeredWaitHandle;
            public Action<TimerHandle> DisposeTimer;

            public void Dispose()
            {
                DisposeTimer(this);
            }
        }
        private List<TimerHandle> m_timers = new List<TimerHandle>();
    }
}

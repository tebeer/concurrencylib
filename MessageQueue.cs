﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace ConcurrencyLib
{
    internal class MessageQueue : IMessageQueue
    {
        public MessageQueue()
        {
            Start();
        }

        private void Start()
        {
            m_running = true;
            m_pendingList = new List<MessageHandler>(1024);
            m_processingList = new List<MessageHandler>(1024);
        }

        public void Stop()
        {
            lock (m_queueLock)
            {
                // Stop queuing messages but process queue to finish 
                m_running = false;
            }
        }

        public void Dispose()
        {
        }

        public bool Enqueue(MessageHandler handler)
        {
            bool process = false;
            lock (m_queueLock)
            {
                if (!m_running)
                    return false;

                m_pendingList.Add(handler);
                if(!m_waitingToProcess)
                {
                    m_waitingToProcess = true;
                    process = true;
                }
            }
            if(process)
                ThreadPool.QueueUserWorkItem(new WaitCallback(ProcessMessages));

            return true;
        }

        private async void ProcessMessages(object state)
        {
            sw.Reset();
            sw.Start();

            lock(m_queueLock)
            {
                if (m_pendingList.Count == 0)
                {
                    m_waitingToProcess = false;
                    return;
                }

                // Swap the lists
                // ProcessingList is empty
                var temp = m_processingList;
                m_processingList = m_pendingList;
                m_pendingList = temp;
            }

            int processedCount = m_processingList.Count;

            for (int i = 0; i < processedCount; ++i)
            {
                var task = m_processingList[i]();
                if (task != null)
                    await task;
            }

            m_processingList.Clear();

            long elapsed = sw.ElapsedTicks;
            m_totalMessages += processedCount;
            m_totalTicks += elapsed;

            m_processTimes++;

            //if((m_totalMessages % 1000000) == 0)
            //Console.WriteLine(string.Format("{0} msgs | {1} ticks, tot: {2} | {3} ms times: {4}",
            //    processedCount, elapsed, m_totalMessages, m_totalTicks * 1000 / Stopwatch.Frequency, m_processTimes));

            lock (m_queueLock)
            {
                if (m_pendingList.Count == 0)
                    m_waitingToProcess = false;
                else
                    ThreadPool.QueueUserWorkItem(new WaitCallback(ProcessMessages));
            }
        }

        private Stopwatch sw = new Stopwatch();

        private long m_processTimes;
        private long m_totalMessages;
        private long m_totalTicks;

        private readonly object m_queueLock = new object();
        private List<MessageHandler> m_pendingList;
        private List<MessageHandler> m_processingList;

        private bool m_waitingToProcess;
        private bool m_running;
    }

}

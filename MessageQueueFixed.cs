﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace ConcurrencyLib
{

    internal class MessageQueueFixed : IMessageQueue
    {
        public MessageQueueFixed(int msec, Action<int> updateFunc)
        {

            //m_fixedUpdate = FixedUpdatePool.AddAction(msec, () =>
            //{
            //    ProcessMessages();
            //    updateFunc(msec);
            //});
            
            // TODO: maybe use a pool of timers and process multiple MessageQueues at once
            // OR create a few dedicated threads
            int currentTick = Environment.TickCount;
            m_timer = new Timer((o) =>
            {
                while (true)
                {
                    currentTick += msec;
                    ProcessMessages();
                    updateFunc(msec);
            
                    if (!m_running)
                        return;
            
                    int ahead = currentTick - Environment.TickCount;
                    if (ahead > 0)
                    {
                        m_timer.Change(ahead, Timeout.Infinite);
                        break;
                    }
                }
            }, null, msec, Timeout.Infinite);

            Start();
        }

        private void Start()
        {
            m_running = true;
            m_pendingList = new List<MessageHandler>(1024);
            m_processingList = new List<MessageHandler>(1024);
        }

        public void Stop()
        {
            lock (m_queueLock)
            {
                // Stop queuing messages but process queue to finish 
                m_running = false;
            }
        }

        public void Dispose()
        {
            //m_fixedUpdate.Dispose();
            m_timer.Dispose();
        }


        public bool Enqueue(MessageHandler handler)
        {
            lock (m_queueLock)
            {
                if (!m_running)
                    return false;

                m_pendingList.Add(handler);
            }

            return true;
        }

        private void ProcessMessages()
        {
            sw.Reset();
            sw.Start();

            lock(m_queueLock)
            {
                if (m_pendingList.Count == 0)
                    return;

                // Swap the lists
                // ProcessingList is empty
                var temp = m_processingList;
                m_processingList = m_pendingList;
                m_pendingList = temp;
            }

            int processedCount = m_processingList.Count;

            for (int i = 0; i < processedCount; ++i)
            {
                var task = m_processingList[i]();
                if (task != null)
                    throw new Exception("Async handler not allowed with fixed message queue");
            }

            m_processingList.Clear();

            long elapsed = sw.ElapsedTicks;
            m_totalMessages += processedCount;
            m_totalTicks += elapsed;

            m_processTimes++;

            //if((m_totalMessages % 1000000) == 0)
            //Console.WriteLine(string.Format("{0} msgs | {1} ticks, tot: {2} | {3} ms times: {4}",
            //    processedCount, elapsed, m_totalMessages, m_totalTicks * 1000 / Stopwatch.Frequency, m_processTimes));
        }

        private Stopwatch sw = new Stopwatch();

        private long m_processTimes;
        private long m_totalMessages;
        private long m_totalTicks;

        private readonly object m_queueLock = new object();
        private List<MessageHandler> m_pendingList;
        private List<MessageHandler> m_processingList;

        private bool m_running;

        //private FixedUpdate m_fixedUpdate;
        private Timer m_timer;

    }

}

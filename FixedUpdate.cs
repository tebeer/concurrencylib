﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ConcurrencyLib
{
    public class FixedUpdate : IDisposable
    {
        internal FixedUpdate(FixedUpdateProcessor processor, Action action)
        {
            m_processor = processor;
            m_action = action;

            m_processor.AddProcess(m_action);
        }

        public void Dispose()
        {
            m_processor.RemoveProcess(m_action);
        }

        private FixedUpdateProcessor m_processor;
        private Action m_action;
    }
}
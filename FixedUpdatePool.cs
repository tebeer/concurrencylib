﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ConcurrencyLib
{
    public static class FixedUpdatePool
    {
        //public FixedUpdatePool(int count)
        //{
        //    m_processors = new FixedUpdateProcessor[count];
        //}

        public static FixedUpdate AddAction(int interval, Action action)
        {
            if (m_processors[currentIndex] == null)
                m_processors[currentIndex] = new FixedUpdateProcessor(interval);

            return new FixedUpdate(m_processors[currentIndex++], action);
        }

        private static FixedUpdateProcessor[] m_processors = new FixedUpdateProcessor[8];
        private static int currentIndex;
    }
}
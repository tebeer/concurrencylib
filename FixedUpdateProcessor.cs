﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace ConcurrencyLib
{
    /// <summary>
    /// Uses a dedicated thread to process actions at specific intervals
    /// </summary>
    internal class FixedUpdateProcessor : IDisposable
    {
        public FixedUpdateProcessor(int msInterval)
        {
            m_msInterval = msInterval;

            m_updatedList = new List<Action>();
            m_processList = new Action[0];
            
            m_running = true;
            m_thread = new Thread(Loop);
            m_thread.Start();
        }

        public void AddProcess(Action action)
        {
            Console.WriteLine("AddProcess");
            lock (m_processLock)
            {
                m_updatedList.Add(action);
                m_changed = true;
            }
        }

        public void RemoveProcess(Action action)
        {
            lock (m_processLock)
            {
                m_updatedList.Remove(action);
                m_changed = true;
            }
        }

        private void Loop()
        {
            //long frequency = Stopwatch.Frequency;
            //long tickInterval = m_msInterval * frequency / 1000;
            //long currentTime = Stopwatch.GetTimestamp();

            int iterations = 0;
            var currentTime = Environment.TickCount;

            while (m_running)
            {
                // Update process list
                lock (m_processLock)
                {
                    if (m_changed)
                    {
                        m_processList = m_updatedList.ToArray();
                        m_changed = false;
                    }
                }

                // Process a frame
                //currentTime += tickInterval;
                currentTime += m_msInterval;

                for (int i = 0; i < m_processList.Length; ++i)
                    m_processList[i]();

                var ahead = currentTime - Environment.TickCount;
                // Sleep if ahead
                if (ahead > 0)
                    Thread.Sleep(ahead);
                // Skip to front if too much behind
                else if (ahead < -2 * m_msInterval)
                {
                    currentTime = Environment.TickCount;
                }

                //var ahead = currentTime - Stopwatch.GetTimestamp();
                //if (ahead > 0)
                //{
                //    // Wait until behind again
                //    int msAhead = (int)(ahead * 1000 / frequency / m_msInterval);
                //    Thread.Sleep(msAhead);
                //}
            }
        }

        public void Dispose()
        {
            m_running = false;
        }

        private bool m_running;
        private int m_msInterval;
        private Thread m_thread;
        private object m_processLock = new object();
        private List<Action> m_updatedList;
        private Action[] m_processList;
        private bool m_changed;
    }
}
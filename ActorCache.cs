﻿using System;
using System.Collections.Concurrent;

namespace ConcurrencyLib
{
    internal abstract class ActorCacheBase
    {
        public abstract ActorRef GetOrCreateActor(int id);
        public abstract bool TryGetActor(int id, out ActorRef actorRef);
        public abstract ActorRef CreateActor();
        public abstract bool ActorExists(int id);

        public abstract bool Remove(int actorID);
    }

    internal class ActorCache<T> : ActorCacheBase where T : Actor, new()
    {
        public ActorCache(ActorSystem system)
        {
            m_system = system;
        }

        public int Count()
        {
            return m_actors.Count;
        }
        
        public override ActorRef GetOrCreateActor(int id)
        {
            var actor = m_actors.GetOrAdd(id, i => LazyActor(id)).Value;
            return new ActorRef(actor);
        }

        public override bool TryGetActor(int id, out ActorRef actorRef)
        {
            Lazy<T> lazyActor;
            if (m_actors.TryGetValue(id, out lazyActor))
            {
                actorRef = new ActorRef(lazyActor.Value);
                return true;
            }

            actorRef = default(ActorRef);
            return false;
        }

        public override ActorRef CreateActor()
        {
            // Find an ID
            int id = 1;
            while (true)
            {
                var lazy = LazyActor(id);
                if (m_actors.TryAdd(id, lazy))
                    return new ActorRef(lazy.Value);
                id++;
            }
        }

        public override bool Remove(int id)
        {
            Lazy<T> lazyActor;
            return m_actors.TryRemove(id, out lazyActor);
        }

        public override bool ActorExists(int id)
        {
            return m_actors.ContainsKey(id);
        }

        private Lazy<T> LazyActor(int id)
        {
            return new Lazy<T>(() => Actor.CreateActor<T>(this, id));
        }

        private ConcurrentDictionary<int, Lazy<T>> m_actors = new ConcurrentDictionary<int, Lazy<T>>();
        private ActorSystem m_system;
    }
}

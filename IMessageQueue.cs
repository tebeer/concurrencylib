﻿using System.Threading.Tasks;

namespace ConcurrencyLib
{
    public delegate Task MessageHandler();

    internal interface IMessageQueue
    {
        void Stop();
        void Dispose();
        bool Enqueue(MessageHandler handler);
    }
}

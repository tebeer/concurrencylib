﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace ConcurrencyLib
{
    public class ActorSystem
    {
        public ActorRef GetOrCreateActor<T>(int id) where T : Actor, new()
        {
            return GetCache<T>().GetOrCreateActor(id);
        }

        public bool TryGetActor<T>(int id, out ActorRef actorRef) where T : Actor, new()
        {
            return GetCache<T>().TryGetActor(id, out actorRef);
        }

        public ActorRef CreateActor<T>() where T : Actor, new()
        {
            return GetCache<T>().CreateActor();
        }

        public bool ActorExists<T>(int id) where T : Actor, new()
        {
            return GetCache<T>().ActorExists(id);
        }

        private ActorCache<T> GetCache<T>() where T : Actor, new()
        {
            return ((Lazy<ActorCache<T>>)m_caches.GetOrAdd(typeof(T), LazyCache<T>)).Value;
        }

        private Lazy<ActorCache<T>> LazyCache<T>(Type t) where T : Actor, new()
        {
            return new Lazy<ActorCache<T>>(() => new ActorCache<T>(this));
        }

        private ConcurrentDictionary<Type, object> m_caches = new ConcurrentDictionary<Type, object>();
    }

}
